# NVIDIA L4T
## Linux for Tegra R32.2.2
This is the top-level repository for Jetson TX2 **kernel** and **Device Tree**.

##  System Requirements
* Host computer running Ubuntu Linux version 16.04 or 18.04 is recommended.

## Get and build the software

### Getting the toolchain
```bash
$ cd $HOME
$ mkdir -p devel && cd devel
$ mkdir jetpack_4.2.2 && cd jetpack_4.2.2
$ export JETPACK_422_PATH=$HOME/devel/jetpack_4.2.2
$ wget https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu.tar.xz
$ tar xf gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu.tar.xz
```

### Getting the source code
```bash
$ cd $JETPACK_422_PATH
$ git clone -b patches-l4t-r32.2.2 git@bitbucket.org:realtimeml/l4t-linux-4.9.git
$ git -C l4t-linux-4.9/ submodule update --init --recursive
```
### Build the software
#### A. kernel Linux
In order to compile the kernel please follow these steps.

###### A.1. Specify the path to the toolchain and architecture:

```bash
$ export CROSS_COMPILE=$JETPACK_422_PATH/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu/bin/aarch64-linux-gnu-
$ export ARCH=arm64
```

###### A.2. Define and create destination directories.
```bash
$ cd $JETPACK_422_PATH/l4t-linux-4.9
$ export TEGRA_KERNEL_OUT=`pwd`/kernel/images
$ export TEGRA_MODULES_OUT=`pwd`/kernel/modules
$ export TEGRA_HEADERS_OUT=`pwd`/kernel/headers
$ mkdir -p $TEGRA_KERNEL_OUT $TEGRA_MODULES_OUT $TEGRA_HEADERS_OUT
```
###### A.3. Clean your kernel and configuration
```bash
$ make -C kernel/kernel-4.9/ mrproper
```

###### A.4. Configure your kernel

```bash
$ ls -lrta kernel/kernel-4.9/arch/arm64/configs
$ make -C kernel/kernel-4.9/ O=$TEGRA_KERNEL_OUT tegra_b102_defconfig
$ make -C kernel/kernel-4.9/ O=$TEGRA_KERNEL_OUT menuconfig
```

__Optional__

Specify a local version : this is going to append to the kernel release.

In Kernel Configuration GUI, Go to General setup -> Local version - append to kernel release, then enter and add a string value like below:

```
-tegra-r32.2.2
```

######  A.5. Compile kernel, device tree, modules and headers.

```bash
$ make -C kernel/kernel-4.9/ O=$TEGRA_KERNEL_OUT zImage -j$(nproc)
$ make -C kernel/kernel-4.9/ O=$TEGRA_KERNEL_OUT dtbs -j$(nproc)
$ make -C kernel/kernel-4.9/ O=$TEGRA_KERNEL_OUT modules -j$(nproc)
$ make -C kernel/kernel-4.9/ O=$TEGRA_KERNEL_OUT modules_install INSTALL_MOD_PATH=$TEGRA_MODULES_OUT
$ make -C kernel/kernel-4.9/ O=$TEGRA_KERNEL_OUT headers_install INSTALL_HDR_PATH=$TEGRA_HEADERS_OUT
```
